package ocaV2;

import java.util.*;

public class oca {
	static String tauler[] = new String[64];
	static Scanner a = new Scanner(System.in);
	static int posJugador1 = 0;
	static int posJugador2 = 0;
	static int jugador = 1;
	static String guanyador = "";
	static boolean iniciado = false;

	public static void main(String[] args) {
		mostrarMenu();
		int eleccio = a.nextInt();
		while (eleccio != 0) {
			switch (eleccio) {
			case 1:
				inicialitzarJoc();
				iniciado = true;
				break;
			case 2:
				if (iniciado  == false) {
					System.out.println("El joc no s'ha inciat. Inicia el tauler primer!");
				} else {
					veureTaulell();
				}
				break;
			case 3:
				if (iniciado == false) {
					System.out.println("El joc no s'ha inciat. Inicia el tauler primer!");
				} else {
					jugar();
				}
				break;
			default:
				System.out.println("Has escollit una opci� err�nia. Prova un altre cop:");
			}
			mostrarMenu();
			eleccio = a.nextInt();
		}
	}

	public static void mostrarMenu() {
		System.out.println("1.-	Inicialitzar joc");
		System.out.println("2.-	Visualitzar tauler final");
		System.out.println("3.-	Jugar");
		System.out.println("0.-	Sortir");
	}

	public static void inicialitzarJoc() {
		posJugador1 = 0;
		posJugador2 = 0;
		guanyador = "";
		crearTauler();

		System.out.println();
	}

	public static void crearTauler() {
		for (int i = 0; i < tauler.length; i++) {
			if (i == 5 || i == 9 || i == 14 || i == 18 || i == 23 || i == 27 || i == 32 || i == 36 || i == 41 || i == 45
					|| i == 50 || i == 54 || i == 59 || i == 63) {
				tauler[i] = " [OCA] "; // Todas las casillas anteriores son ocas
			} else if (i == 0) {
				tauler[i] = " [INICI] ";
			} else if (i == 26 || i == 53) {
				tauler[i] = " [DAUS] "; // Casillas dados
			} else if (i == 42) {
				tauler[i] = " [LABERINT] "; // Casilla laberinto
			} else if (i == 58) {
				tauler[i] = " [MORT] "; // Casilla muerte
			} else {
				tauler[i] = " [" + String.valueOf(i) + "] ";
			}
		}
		veureTaulell();
	}

	public static void jugar() {
		while (guanyador == "") {
			tirarDaus();
			comprovarCasella();
			veureTaulell();
			System.out.println();
			System.out.println();
			System.out.println("El jugador 1 est� en la posici�n " + posJugador1);
			System.out.println("El jugador 2 est� en la posici�n " + posJugador2);
		}
		System.out.println("Ha guanyat el jugador " + guanyador);
		System.out.println();
	}

	public static void tirarDaus() {
		System.out.println("\nEscriu quantes posicions vols avan�ar:");
		int tirada = a.nextInt();
		while (tirada < 1 || tirada > 6) {
			System.out.println("El dau nom�s t� sis cares. No pots escollir m�s, ni menys!\nTorna a provar");
			tirada = a.nextInt();
		}
		moureJugador(tirada);
	}

	public static void moureJugador(int num) {
		if (jugador == 1) {
			posJugador1 += num;
		} else if (jugador == 2) {
			posJugador2 += num;
		}
	}

	public static void comprovarCasella() {
		int sobra = 0;
		ArrayList<Integer> llistaPosOca = new ArrayList<>(
				Arrays.asList(5, 9, 14, 18, 23, 27, 32, 36, 41, 45, 50, 54, 59));
		if (posJugador1 > 63) {
			sobra = posJugador1 - 63;
			posJugador1 = 63 - sobra;
		}
		if (posJugador2 > 63) {
			sobra = posJugador2 - 63;
			posJugador2 = 63 - sobra;
		}
		if (jugador == 1) {
			if (posJugador1 == 58) {
				posJugador1 = 0;
				System.out.println();
				System.out.println("Has mort!");
				jugador = 2;
			} else if (posJugador1 == 42) {
				posJugador1 = 30;
				System.out.println();
				System.out.println("T'has perdut pel laberint i has baixat fins la posici� 30");
				jugador = 2;
			} else if (posJugador1 == 26) {
				posJugador1 = 53;
				System.out.println();
				System.out.println("La sort ha caigut sobre tu i has avan�at fins la casella 53");
				jugador = 1;
			} else if (posJugador1 == 53) {
				posJugador1 = 26;
				System.out.println();
				System.out.println("Avui no �s el teu millor dia. Has baixat fins la casella 26");
				jugador = 1;
			} else if (posJugador1 == 59) {
				posJugador1 = 63;
				guanyador = "Jugador1";
			} else if (llistaPosOca.contains(posJugador1)) {
				System.out.println();
				System.out.println("Has caigut en una OCA! Torna a tirar");
				int index = llistaPosOca.indexOf(posJugador1);
				posJugador1 = llistaPosOca.get(index + 1);
				jugador = 1;
			} else {
				jugador = 2;
			}
		} else if (jugador == 2) {
			if (posJugador2 == 58) {
				posJugador2 = 0;
				System.out.println();
				System.out.println("Has mort!");
				jugador = 1;
			} else if (posJugador2 == 42) {
				posJugador2 = 30;
				System.out.println();
				System.out.println("T'has perdut pel laberint i has baixat fins la posici� 30");
				jugador = 1;
			} else if (posJugador2 == 26) {
				posJugador2 = 53;
				System.out.println();
				System.out.println("La sort ha caigut sobre tu i has avn�at fins la casella 53");
				jugador = 2;
			} else if (posJugador2 == 53) {
				posJugador2 = 26;
				System.out.println();
				System.out.println("Avui no �s el teu millor dia. Has baixat fins la casella 26");
				jugador = 2;
			} else if (posJugador2 == 59) {
				posJugador2 = 63;
				guanyador = "Jugador2";
			} else if (llistaPosOca.contains(posJugador2)) {
				System.out.println();
				System.out.println("Has caigut en una OCA! Torna a tirar");
				int index = llistaPosOca.indexOf(posJugador2);
				posJugador2 = llistaPosOca.get(index + 1);
				jugador = 2;
			} else {
				jugador = 1;
			}
		}
		System.out.println();
		System.out.println("Torn del jugador " + jugador);
		System.out.println();
	}

	public static void veureTaulell() {
		System.out.println("Aix� est� el tauler:");
		System.out.println();
		for (int i = 0; i < tauler.length; i++) {
			System.out.print(tauler[i] + " ");
			if (i == posJugador1) {
				System.out.print("-> Jugador1 ");
			}
			if (i == posJugador2) {
				System.out.print("-> Jugador2 ");
			}

			if (i % 16 == 0) {
				System.out.println();
				System.out.println();
			}
		}
		System.out.println();
	}
}